# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [7.4.2](https://gitlab.com/kohanajs/kohanajs/compare/v7.4.1...v7.4.2) (2023-07-17)


### Bug Fixes

* json with querystring parsed as html ([02513a8](https://gitlab.com/kohanajs/kohanajs/commit/02513a8e05abe6836373f7d4693ceba9581d8231))

### [7.4.1](https://gitlab.com/kohanajs/kohanajs/compare/v7.4.0...v7.4.1) (2023-05-31)

## [7.4.0](https://gitlab.com/kohanajs/kohanajs/compare/v7.3.0...v7.4.0) (2023-02-21)


### Features

* use latest kohanajs core-mvc ([809e9e1](https://gitlab.com/kohanajs/kohanajs/commit/809e9e1162689a9e49bbb7c15060c08017e9cf74))

### [7.2.1](https://gitlab.com/kohanajs/kohanajs/compare/v8.0.0...v7.2.1) (2022-09-16)

### [7.2.1](https://gitlab.com/kohanajs/kohanajs/compare/v8.0.0...v7.2.1) (2022-09-16)

## [7.2.0](https://gitlab.com/kohanajs/kohanajs/compare/v7.1.0...v8.0.0) (2022-05-30)


### Features

* ORM write retry to prevent id collision

### Features

* ORM write retry to prevent id collision ([0ebe637](https://gitlab.com/kohanajs/kohanajs/commit/0ebe637f2f614184f8b36422331025722f30f99d))

## [7.1.0](https://gitlab.com/kohanajs/kohanajs/compare/v7.0.0...v7.1.0) (2022-05-27)


### Features

* ORM.countBy and countWith ([85863cf](https://gitlab.com/kohanajs/kohanajs/commit/85863cf6c288b613aa6194e8f3538650e6e31691))


### Bug Fixes

* CI ([61a2283](https://gitlab.com/kohanajs/kohanajs/commit/61a228302a59ad5ab2476e499c797dcdcba7d376))

## [7.0.0](https://gitlab.com/kohanajs/kohanajs/compare/v6.1.4...v7.0.0) (2022-03-09)


### ⚠ BREAKING CHANGES

* remove system path folder, remove version

### Features

* add defer script and scripts default layout data ([78c69db](https://gitlab.com/kohanajs/kohanajs/commit/78c69dbdef8e4a83c13140840ba113f356593799))


### Bug Fixes

* tests and default layout data ([54a16dc](https://gitlab.com/kohanajs/kohanajs/commit/54a16dc5fd3756b5ae0897618dfe0a6ca09c2b45))


* remove system path folder, remove version ([644e192](https://gitlab.com/kohanajs/kohanajs/commit/644e192daa1219feffdc7f5b18621d48376a809b))

### 6.1.4 (2021-10-19)


### Bug Fixes

* ORM field with Boolean type will become string after save. ([f3d13d4](https://gitlab.com/kohanajs/kohanajs/commit/f3d13d4c9c30d54faf8f882612f0989c188eaefb))

## [6.1.3] - 2021-10-16
### Fixed
- defaultViewData not assign to default layout.

## [6.1.2] - 2021-10-13
### Fixed
- test files

## [6.1.1] - 2021-10-13
### Fixed
- fix config loading error when config file not exist but provided fallback object.

## [6.1.0] - 2021-10-06
### Added
- Default Database Driver

## [6.0.1] - 2021-10-05
### Changed
- KohanaJS init.js loading sequence, load require first, then system, then module

## [6.0.0] - 2021-09-07
### Changed
- update KohanaJS version number

## [6.0.0] - 2021-09-07
### Changed
- Move ControllerMixinMultipartForm to @kohanajs/mod-form

## [5.2.2] - 2021-09-07
### Fixed
- Fix error when ORM eagerLoad not provide "with" Array.

## [5.2.1] - 2021-09-07
### Added
- create CHANGELOG.md